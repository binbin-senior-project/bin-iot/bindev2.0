import subprocess as sp
import re
from BluetoothctlWrapper import Bluetoothctl

# format of the address
r = re.compile('..:..:..:..:..:..')

# crate bluetoothctl object
bl = Bluetoothctl()
currentAddress = ''

# numCon = number of connection
def controlConnection():
  global currentAddress
  numCon, adds = numberOfConnection()
  # print(numCon, adds, )
  if numCon == 1:
    currentAddress = adds[0]
  elif numCon >1:
    for a in adds:
      print(a != currentAddress,a,currentAddress)
      if a != currentAddress:
        bl.disconnectWithAddress(a)
  elif numCon == 0:
    currentAddress = ''

def numberOfConnection():
  stdoutResult = sp.getoutput("hcitool con")
  addressResult = r.findall(stdoutResult)
  return len(addressResult), addressResult

def disconnectCurrent():
  bl.disconnectWithAddress(currentAddress)
  currentAddress = ''
# test code
# from time import sleep
# while True:
#   print('Adress: ',currentAddress)
#   controlConnection()
#   sleep(2)