import os
os.popen('sudo hciconfig hci1 reset')
import sys

def get_parent_dir(n=1):
	current_path = os.path.dirname(os.path.abspath(__file__))
	for k in range(n):
		current_path = os.path.dirname(current_path)
	return current_path


bleUtil_path = os.path.join(get_parent_dir(0),'BLEUtility')
sys.path.append(bleUtil_path)
print("register path: ",bleUtil_path)


bottleUtil_path = os.path.join(get_parent_dir(0),'BottleUtility')
sys.path.append(bottleUtil_path)
print("register path: ",bottleUtil_path)


import sys
from time import sleep
import dbus, dbus.mainloop.glib
from gi.repository import GLib
from Advertisement import Advertisement
from Advertisement import register_ad_cb, register_ad_error_cb

from Gatt import Service, Characteristic
from Gatt import register_app_cb, register_app_error_cb

from BluetoothctlWrapper import *
from Parameter import *

# do not forget to register IR sensor
from IRSensor import initialIRSensor, isBottle, detectSize
from ConveyerBelt import findObject, inputObject, selectBin, measureSize

# import camera
from Camera import takePicture

# load photo detection library
# comment when test bluetooth

# from TrashType import *
# yolo = loadModel()
# img_path = 'test.jpg'
# predict = detectTrash(img_path,yolo)

# comment when test bluetooth

from ImageClassification import detect_object, decode_result
# img_path = './BottleUtility/glass.jpg'
# detect_object(img_path)

from gpiozero import CPUTemperature

from Helper import controlConnection, numberOfConnection

mainloop = None
count = 0



class TxCharacteristic(Characteristic):
    
    # def disconnectLast(self):
    #     global count
    #     bl = Bluetoothctl()
    #     bl.disconnect()
    #     self.value = [10]
    #     count = 0
    #     self.StopNotify()

    def __init__(self, bus, index, service):
        Characteristic.__init__(self, bus, index, UART_TX_CHARACTERISTIC_UUID,
                                ["notify"], service)
        self.notifying = False
        self.value = [10]

    def checkState(self):
        controlConnection()

        global count
        global yolo
        
        numCon,adds = numberOfConnection()
        print("Wait for a object")
        if numCon == 0:
            print("There is no device so stop notifying")
            self.value = [10]
            count = 0
            # self.StopNotify()
            return self.notifying
            
        # try:
        # find the inputed object
        inObject = inputObject()
        if inObject == True:

            # find the object on the conveyer belt
            anyObject = findObject()
            if anyObject == True:
                
                print("Detect Trash")
                count = 0
                # img_path = 'test.jpg'
                img_path = 'glass.jpg'
                # takePicture(img_path)
                preClass = detect_object(img_path)
                trash, isObject = decode_result(preClass)
                
                size = ''
                # if isObject:
                #     size = str(measureSize())

                trashR = trash + size
                value = []
                for c in trashR:
                    value.append(dbus.Byte(c.encode()))
                self.value = value
                
                self.PropertiesChanged(GATT_CHRC_IFACE, {"Value":self.value}, [])
                print("Send bluetooth")
                print(self.value)
                    
                
                # print()
                # print("The trash is",trash.upper())
                # print()
                # selectBin(trash)
                # sleep(10)

                return self.notifying                      

            else:
                
                count += 30
                self.value = [10]
                print(count)

        else:
            count += 1
            self.value = [10]
            print(count)
    
        if count >= 30:
            print("Idel too long, on the conveyer")
            # bl = Bluetoothctl()s
            # bl.disconnect()
            self.value = [10]

        # except Exception as e :
        #     print("!!!! FOUND AN ERROR !!!!")
        #     print(e)
        #     s = " "
        #     value = []
        #     for c in s:
        #         value.append(dbus.Byte(c.encode()))
                
        #     self.value = value

       
        self.PropertiesChanged(GATT_CHRC_IFACE, {"Value":self.value}, [])
        print("Send bluetooth")
        print(self.value)
        # print(self.notifying)
        return self.notifying    
    
    def StartNotify(self):
        global count
        if self.notifying:
            return
        self.notifying = True
        self.value = [10]
        count = 0
        self.add_timeout(1000, self.checkState)
    def StopNotify(self):
        self.notifying = False
 
class RxCharacteristic(Characteristic):
    def __init__(self, bus, index, service):
        Characteristic.__init__(self, bus, index, UART_RX_CHARACTERISTIC_UUID,
                                ['write','read'], service)
        self.value = [10]

    def WriteValue(self, value, options):
        print('remote: {}'.format(bytearray(value).decode()))
        self.value = value
        # if bytearray(value).decode() == 'fuck':
        #     bl = Bluetoothctl()
        #     bl.disconnect()
        #     pass

    def ReadValue(self, options):
        s = "Connected"
        value = []
        for c in s:
            value.append(dbus.Byte(c.encode()))

        self.value = value

        # print("Connections = ",numberOfConnection())
        # while numberOfConnection() > 1:
        #     print("muda")
        #     bl = Bluetoothctl()
        #     bl.disconnect()

        return self.value

class UartService(Service):
    def __init__(self, bus, index):
        Service.__init__(self, bus, index, UART_SERVICE_UUID, True)
        self.add_characteristic(TxCharacteristic(bus, 0, self))
        self.add_characteristic(RxCharacteristic(bus, 1, self))

class Application(dbus.service.Object):
    def __init__(self, bus):
        self.path = '/'
        self.services = []
        dbus.service.Object.__init__(self, bus, self.path)
 
    def get_path(self):
        return dbus.ObjectPath(self.path)
 
    def add_service(self, service):
        self.services.append(service)
 
    @dbus.service.method(DBUS_OM_IFACE, out_signature='a{oa{sa{sv}}}')
    def GetManagedObjects(self):
        response = {}
        for service in self.services:
            response[service.get_path()] = service.get_properties()
            chrcs = service.get_characteristics()
            for chrc in chrcs:
                response[chrc.get_path()] = chrc.get_properties()
        return response
 
class UartApplication(Application):
    def __init__(self, bus):
        Application.__init__(self, bus)
        self.add_service(UartService(bus, 0))
 
class UartAdvertisement(Advertisement):
    def __init__(self, bus, index):
        Advertisement.__init__(self, bus, index, 'peripheral')
        self.add_service_uuid(UART_SERVICE_UUID)
        self.add_local_name(LOCAL_NAME)
        self.include_tx_power = True
 
def find_adapter(bus):
    remote_om = dbus.Interface(bus.get_object(BLUEZ_SERVICE_NAME, '/'),
                               DBUS_OM_IFACE)
    objects = remote_om.GetManagedObjects()
    for o, props in objects.items():
        if LE_ADVERTISING_MANAGER_IFACE in props and GATT_MANAGER_IFACE in props:
            return o
        print('Skip adapter:', o)
    return None
 
def main():

    global mainloop
    global adv

    dbus.mainloop.glib.DBusGMainLoop(set_as_default=True)
    bus = dbus.SystemBus()
    adapter = find_adapter(bus)
    if not adapter:
        print('BLE adapter not found')
        return
 
    service_manager = dbus.Interface(
                                bus.get_object(BLUEZ_SERVICE_NAME, adapter),
                                GATT_MANAGER_IFACE)
    ad_manager = dbus.Interface(bus.get_object(BLUEZ_SERVICE_NAME, adapter),
                                LE_ADVERTISING_MANAGER_IFACE)
 
    app = UartApplication(bus)
    adv = UartAdvertisement(bus, 0)
 
    mainloop = GLib.MainLoop()
 
    service_manager.RegisterApplication(app.get_path(), {},
                                        reply_handler=register_app_cb,
                                        error_handler=register_app_error_cb)
    ad_manager.RegisterAdvertisement(adv.get_path(), {},
                                     reply_handler=register_ad_cb,
                                     error_handler=register_ad_error_cb)
    try:
        mainloop.run()
    except KeyboardInterrupt:
        mainloop.quit()
        adv.Release()

 
if __name__ == '__main__':
    main()
