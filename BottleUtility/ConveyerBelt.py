from time import sleep, time
import RPi.GPIO as GPIO
import Pins
# GPIO.cleanup()
GPIO.setmode(GPIO.BCM)

initLeft = False
initRight = False
initConveyer = False

GPIO.setup(Pins.SWITCH_LD_PIN,GPIO.IN) 
GPIO.setup(Pins.SWITCH_LU_PIN,GPIO.IN)
GPIO.setup(Pins.SWITCH_RD_PIN,GPIO.IN) 
GPIO.setup(Pins.SWITCH_RU_PIN,GPIO.IN)  

GPIO.setup(Pins.IR1_PIN,GPIO.IN, pull_up_down=GPIO.PUD_DOWN)
GPIO.setup(Pins.IR2_PIN,GPIO.IN, pull_up_down=GPIO.PUD_DOWN)
GPIO.setup(Pins.IR3_PIN,GPIO.IN, pull_up_down=GPIO.PUD_DOWN)
GPIO.setup(Pins.IR4_PIN,GPIO.IN, pull_up_down=GPIO.PUD_DOWN)

def inputObject():
  # print("Finding the inputed object")
  front_ir_sensor = GPIO.input(Pins.IR1_PIN)
  if front_ir_sensor == 0:
    print("Have a object")
    return True
  else:
    # print("Have no object")
    return False

def initConveyerBelt():
  global initConveyer
  GPIO.setup(Pins.DIR_PIN3,GPIO.OUT)
  GPIO.setup(Pins.STEP_PIN3,GPIO.OUT)
  
  GPIO.output(Pins.DIR_PIN3, GPIO.HIGH)
  initConveyer = True

def findObject(feed_time = 30):
  global initConveyer

  if initConveyer == False:
    initConveyerBelt()

  t_end = time() + feed_time
  print("Finding a object on the conveyer belt ...")
  while time() < t_end:
    end_ir_sensor = GPIO.input(Pins.IR4_PIN)
    if end_ir_sensor == 1:
      GPIO.output(Pins.STEP_PIN3, GPIO.HIGH)
      sleep(0.0002)
      GPIO.output(Pins.STEP_PIN3, GPIO.LOW)
      sleep(0.0002)
      
    else:
      print("Have a object")
      return True

  print("Have no object")
  return False

def initLeftSelector(setDefault = False):
  global initLeft
  print(initLeft)
  GPIO.setup(Pins.DIR1_PIN,GPIO.OUT)
  GPIO.setup(Pins.STEP1_PIN,GPIO.OUT)

  GPIO.output(Pins.DIR1_PIN, GPIO.HIGH)
  if setDefault == True:
    setDefaultLeftSelector()
  initLeft = True

def setDefaultLeftSelector():
  global initLeft
  GPIO.output(Pins.DIR1_PIN, GPIO.HIGH)
  degree = 50
  n_step = (int) (degree * 1600 * 51 / 360)
  for step in range(n_step):
      GPIO.output(Pins.STEP1_PIN, GPIO.HIGH)
      sleep(0.00001)
      GPIO.output(Pins.STEP1_PIN, GPIO.LOW)
      sleep(0.00001)
      if GPIO.input(Pins.SWITCH_LU_PIN) == 1:
        break
  sleep(0.1)        


def moveLeftSelector(degree, clockwise = True, breakSwitch = False):
  global initLeft
  if initLeft != True:
    initLeftSelector(True)
  if clockwise == True: # up
    current_switch = Pins.SWITCH_LU_PIN
    GPIO.output(Pins.DIR1_PIN, GPIO.HIGH)
  else: # down
    current_switch = Pins.SWITCH_LD_PIN
    GPIO.output(Pins.DIR1_PIN, GPIO.LOW)

  nRound = (int) (degree * 1600 * 51 / 360)
  for i in range(nRound):
      GPIO.output(Pins.STEP1_PIN, GPIO.HIGH)
      sleep(0.00002)
      GPIO.output(Pins.STEP1_PIN, GPIO.LOW)
      sleep(0.00002)
      if (breakSwitch == True) and (GPIO.input(current_switch)) == 1:
        break

def initRightSelector(setDefault = False):
  global initRight
  GPIO.setup(Pins.DIR2_PIN,GPIO.OUT)
  GPIO.setup(Pins.STEP2_PIN,GPIO.OUT)

  GPIO.output(Pins.DIR2_PIN, GPIO.HIGH)
  if setDefault == True:
    setDefaultRightSelector()
  initRight = True

def setDefaultRightSelector():

  GPIO.output(Pins.DIR2_PIN, GPIO.LOW)
  degree = 50
  n_step = (int) (degree * 1600 * 51 / 360)
  for i in range(n_step):
      GPIO.output(Pins.STEP2_PIN, GPIO.HIGH)
      sleep(0.00002)
      GPIO.output(Pins.STEP2_PIN, GPIO.LOW)
      sleep(0.00002)
      if GPIO.input(Pins.SWITCH_RU_PIN) == 1:
        break
  sleep(0.1)        

def moveRightSelector(degree, clockwise = True, breakSwitch = False):
  global initRight
  if initRight != True:
    initRightSelector(True)
  if clockwise == True: #down 
    GPIO.output(Pins.DIR2_PIN, GPIO.HIGH)
    current_switch = Pins.SWITCH_RD_PIN
  else: # up
    GPIO.output(Pins.DIR2_PIN, GPIO.LOW)
    current_switch = Pins.SWITCH_RU_PIN

  nRound = (int) (degree * 1600 * 51 / 360)
  for i in range(nRound):
      GPIO.output(Pins.STEP2_PIN, GPIO.HIGH)
      sleep(0.00002)
      GPIO.output(Pins.STEP2_PIN, GPIO.LOW)
      sleep(0.00002)
      if (breakSwitch == True) and (GPIO.input(current_switch) == 1):
        break

def feedConveyer(clockwise = True,duration = 4, noObjectStop = False):
  global initConveyer
  if initConveyer != True:
    initConveyerBelt()
  print(clockwise)
  if clockwise == True:
    # print("Nani")
    GPIO.output(Pins.DIR_PIN3, GPIO.HIGH)
  else:
    GPIO.output(Pins.DIR_PIN3, GPIO.LOW)

  t_end = time() + duration
  while time() < t_end:
      GPIO.output(Pins.STEP_PIN3, GPIO.HIGH)
      sleep(0.00002)
      GPIO.output(Pins.STEP_PIN3, GPIO.LOW)
      sleep(0.00002)
      end_ir_sensor = GPIO.input(Pins.IR4_PIN)
      # if noObjectStop == True and end_ir_sensor == 1:
        # break



def selectBin(trashType):
  # print(trashType)
  if trashType == 'can':
    # print("ora")
    moveRightSelector(30, True, True)
    feedConveyer(True)
    moveRightSelector(30, False, True)
    
  elif trashType == 'glass':
    moveLeftSelector(30, False, True)
    feedConveyer(True)
    moveLeftSelector(30, True, True)
    
  elif trashType == 'plastic':
    feedConveyer(True)
  else:
    feedConveyer(False)

def measureSize():
  # if inputObject():
  #   return -1
  ir4 = GPIO.input(Pins.IR2_PIN)
  ir5 = GPIO.input(Pins.IR3_PIN)
  print("Size: ",ir4,ir5)
  # print(ir4, ir5)
  if ir4 == 0 and ir5 == 0:
    # print("Big")
    return 3
  elif ir4 == 0 and ir5 == 1:
    # print("Medium")
    return 2
  elif ir4 == 1 and ir5 == 1:
    # print("Small")
    return 1
  else:
    # print("Nani")
    return 0
    

# initConveyerBelt()
# while True:
#   GPIO.output(stepPin3, GPIO.HIGH)
#   sleep(0.000001)
#   GPIO.output(stepPin3, GPIO.LOW)
#   sleep(0.000001)


# initRightSelector()
# GPIO.output(dirPin2, GPIO.LOW)
# # GPIO.output(dirPin2, GPIO.LOW)
# while True:
#   GPIO.output(stepPin2, GPIO.HIGH)
#   sleep(0.000001)
#   GPIO.output(stepPin2, GPIO.LOW)
#   sleep(0.000001)

# initLeftSelector()
# GPIO.output(dirPin, GPIO.HIGH)
# i = 0
# while True:
#   print(i)
#   i += 1
#   GPIO.output(stepPin, GPIO.HIGH)
#   sleep(0.000001)
#   GPIO.output(stepPin, GPIO.LOW)
#   sleep(0.000001)
  
# initRightSelector(True)
# moveRightSelector(23, True) # down
# sleep(0.2)
# moveRightSelector(23, False)

# initLeftSelector(True)
# moveLeftSelector(20, False) # clockwise
# sleep(0.2)
# moveLeftSelector(20, True) # anti clockwisess
# selectBin('glass')
# findObject()
# inputObject()
# while True:
# feedConveyer()
  

# findObject()
# while True:
#   print(inputObject())

# while True:
#   ir1 = GPIO.input(IR1Pin)
#   print(ir1)
# GPIO.setup(dirPin3,GPIO.OUT)
# GPIO.output(dirPin3, GPIO.LOW)
# selectBin("fuck")
# feedConveyer(False)

# while True:
#   ir4 = GPIO.input(IR4Pin)
#   ir5 = GPIO.input(IR5Pin)
#   # print(ir4, ir5)
#   if ir4 == 0 and ir5 == 0:
#     print("Big")
#   elif ir4 == 0 and ir5 == 1:
#     print("Medium")
#   elif ir4 == 1 and ir5 == 1:
#     print("Small")
#   else:
#     print("Nani")
 
  
#   sleep(10)

# initRightSelector(True)

# moveRightSelector(22, True,True) # down
# sleep(0.2)
# moveRightSelector(20, False,True)

# initLeftSelector(True)
print("Ora")
# moveLeftSelector(30, False,True) # clockwise
# sleep(0.2)
# moveLeftSelector(30, True,True) # anti clockwise

# selectBin("can")