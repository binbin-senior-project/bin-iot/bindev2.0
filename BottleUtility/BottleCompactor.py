import RPi.GPIO as GPIO
import time
import Pins

GPIO.setmode(GPIO.BCM)
GPIO.setwarnings(False)

class Compactor:
  status = None
  def __init__(self, bottle_compac_pin = Pins.BOTTLE_COMPAC_PIN):
    # set pin to be output
    self.bottle_compac_pin = bottle_compac_pin
    GPIO.setup(self.bottle_compac_pin, GPIO.OUT)
    # GPIO.output(self.bottle_compac_pin,  GPIO.LOW)

  def turn_on(self):
    if self.status == True:
      return
    self.status = True
    GPIO.output(self.bottle_compac_pin,  GPIO.HIGH)
    print("Compactor: Turn On")

  def turn_off(self):
    print(self.status)
    if self.status == False:
      return
    self.status = False
    GPIO.output(self.bottle_compac_pin,  GPIO.LOW)
    print("Compactor: Turn Off")


  def test_relay(self):
    for i in range(100):
      self.turn_on()
      time.sleep(5)
      self.turn_off()
      time.sleep(5)

com = Compactor()
com.turn_off()