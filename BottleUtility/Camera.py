import cv2

# for computer that has no camera
cameratype = 0

# for computer that has a camera
# cameratype = 1




def takePicture(filename):
        # initialize the camera
        cam = cv2.VideoCapture(cameratype)

        # get width and height
        w = cam.get(cv2.CAP_PROP_FRAME_WIDTH)
        h = cam.get(cv2.CAP_PROP_FRAME_HEIGHT)
        # get picture from the webcam
        ret, image = cam.read()
        cam.release() 
        if ret:
                # save picture
                cv2.imwrite(filename,image)
                print("Save completely at "+filename+"("+str(w)+"X"+str(h)+")")

# test module
if __name__ == '__main__':
    takePicture("test.jpg")