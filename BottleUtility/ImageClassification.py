import numpy as np
from numpy import loadtxt
from keras.models import load_model
from keras.applications.inception_v3 import preprocess_input
from keras.preprocessing import image
from keras.models import model_from_yaml
from keras.preprocessing.image import ImageDataGenerator

yaml_file = open('./BottleUtility/Models/Inception3.Architecture.0.99.50.16.True.yaml', 'r')
# yaml_file = open('./Models/Inception3.Architecture.0.9833333333333333.yaml', 'r')
loaded_model_yaml = yaml_file.read()
yaml_file.close()
loaded_model = model_from_yaml(loaded_model_yaml)
# load weights into new model
loaded_model.load_weights("./BottleUtility/Models/Inception3.Weight.0.99.50.16.True.h5")
# loaded_model.load_weights("./Models/Inception3.Weight.0.9833333333333333.h5")
print("Loaded model from disk")
# loaded_model.summary()

def detect_object(img_path):
  # img_path = './BottleUtility/glass.jpg'
  img = image.load_img(img_path, target_size=(299, 299))
  x = image.img_to_array(img)
  x = np.expand_dims(x, axis=0)
  x = preprocess_input(x)

  preClass = loaded_model.predict(x)
  print(preClass)
  return preClass

def decode_result(predicts,threshold = 0):
  predicts = predicts[0]
  index = -1
  prob = 0
  foundObject = False
  for i in range(len(predicts)):
    if predicts[i] > prob  and predicts[i] > threshold:
      prob = predicts[i]
      index = i
      foundObject = True
  
  classes = ['can', 'glass', 'plastic', 'notSure']
  print("decode_result:",classes[index],str(foundObject))
  return classes[index], foundObject


detect_object('trash.jpg')