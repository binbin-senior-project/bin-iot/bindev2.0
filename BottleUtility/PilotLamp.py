import RPi.GPIO as GPIO
import time
import Pins

GPIO.setmode(GPIO.BCM)
GPIO.setwarnings(False)

class PilotLamp:
  status = False
  def __init__(self, pilot_pin = Pins.PILOT_PIN):
    # set pin to be output
    self.pilot_pin = pilot_pin
    GPIO.setup(self.pilot_pin, GPIO.OUT)
    self.turn_off()

  def turn_on(self):
    if self.status == True:
      return
    self.status = True
    GPIO.output(self.pilot_pin,  GPIO.LOW)
    print("PilotLamp: Green")

  def turn_off(self):
    if self.status == False:
      return
    self.status = False
    GPIO.output(self.pilot_pin,  GPIO.HIGH)
    print("PilotLamp: Red")


  def test_relay(self):
    for i in range(100):
      self.turn_on()
      time.sleep(5)
      self.turn_off()
      time.sleep(5)

# com = PilotLamp()
# com.test_relay()
