import os
os.popen('sudo hciconfig hci0 reset')
import sys

def get_parent_dir(n=1):
	current_path = os.path.dirname(os.path.abspath(__file__))
	for k in range(n):
		current_path = os.path.dirname(current_path)
	return current_path


bleUtil_path = os.path.join(get_parent_dir(0),'BLEUtility')
sys.path.append(bleUtil_path)
print("register path: ",bleUtil_path)


bottleUtil_path = os.path.join(get_parent_dir(0),'BottleUtility')
sys.path.append(bottleUtil_path)
print("register path: ",bottleUtil_path)

import sys
from time import sleep
import dbus, dbus.mainloop.glib
from gi.repository import GLib
from Advertisement import Advertisement, register_ad_cb, register_ad_error_cb
from Application import Application

from Gatt import Service, Characteristic, register_app_cb, register_app_error_cb

from Parameter import *
from BluetoothctlWrapper import Bluetoothctl

from ConveyerBelt import findObject, inputObject, selectBin, measureSize

from Camera import takePicture

from ImageClassification import detect_object, decode_result

from BottleCompactor import Compactor
compactor = Compactor()

from PilotLamp import PilotLamp
pilotLamp = PilotLamp()

from gpiozero import CPUTemperature

from Helper import controlConnection, numberOfConnection, disconnectCurrent

import time

from datetime import datetime

from CheckElectric import isCurrentAvalible

mainloop = None
count = 0

class BinbinCharacteristic(Characteristic):

    def __init__(self, bus, index, service):
        Characteristic.__init__(self, bus, index, BINBIN_CHARACTERISTIC_UUID,
                                ["notify"], service)
        self.notifying = True
        self.value = [10]
        self.add_timeout(1000, self.checkState)

    def checkState(self):
        global count
        global yolo
        global compactor
        global pilotLamp
        
        # check current
        if isCurrentAvalible == False:
            pilotLamp.turn_off()
            return

        print("Checking Bluetooth Connection")       
        controlConnection()        
        numCon,adds = numberOfConnection()
        if numCon == 0:
            self.value = [10]
            count = 0
            compactor.turn_off()
            return self.notifying

        print("Wait for input")
        pilotLamp.turn_on()
        inObject = inputObject()
        if inObject == True:
            pilotLamp.turn_off()
            print("Wait for position")
            anyObject = findObject()
            if anyObject == True:
                st = time.time()  
                print("Detect Trash")
                count = 0
                now = datetime.now()
                date_time = now.strftime("%m.%d.%Y_%H.%M.%S")
                img_path = 'TrashImages/trash' + date_time + '.jpg'
                takePicture(img_path)
                preClass = detect_object(img_path)
                trash, isObject = decode_result(preClass)
                if 'glass' not in trash:
                    compactor.turn_on()
                size = ''
                if isObject:
                    size = str(measureSize())

                trashR = trash + size
                value = []
                for c in trashR:
                    value.append(dbus.Byte(c.encode()))
                self.value = value
                et = time.time()
                controlConnection()       
                self.PropertiesChanged(GATT_CHRC_IFACE, {"Value":self.value}, [])
                print("Send bluetooth")
                print(self.value)
                    
                
                print()
                print("The trash is",trashR.upper(),"Time spend:", et-st)
                print()

                selectBin(trash)
                time.sleep(2)
                compactor.turn_off()
                
                return self.notifying            
                      
            else:
                
                count += 30
                self.value = [10]
                print(count)

        else:
            count += 1
            self.value = [10]
            print(count)
    
        if count >= 30:
            print("Idel too long, on the conveyer")
            bl = BluetoothctlWrapper()
            bl.disconnect()
            # disconnectCurrent()
            self.value = [10]
       
        # self.PropertiesChanged(GATT_CHRC_IFACE, {"Value":self.value}, [])
        print("Send bluetooth")
        print(self.value)
        # print(self.notifying)
        return self.notifying    

    def StartNotify(self):
      if self.notifying:
          print('Already notifying, nothing to do')
          return

      self.notifying = True
      self.value = [10]
      count = 0
    

    def StopNotify(self):
      if not self.notifying:
          print('Not notifying, nothing to do')
          return

      self.notifying = False
      self.value = [10]
 

class BinbinService(Service):
  def __init__(self, bus, index):
    Service.__init__(self, bus, index, BINBIN_SERVICE_UUID, True)
    self.add_characteristic(BinbinCharacteristic(bus, 0, self))


class BinbinApplication(Application):
    def __init__(self, bus):
        Application.__init__(self, bus)
        self.add_service(BinbinService(bus, 0))

class BinbinAdvertisement(Advertisement):
    def __init__(self, bus, index):
        Advertisement.__init__(self, bus, index, 'peripheral')
        self.add_service_uuid(BINBIN_SERVICE_UUID)
        self.add_local_name(LOCAL_NAME)
        self.include_tx_power = True

def find_adapter(bus):
    remote_om = dbus.Interface(bus.get_object(BLUEZ_SERVICE_NAME, '/'),
                               DBUS_OM_IFACE)
    objects = remote_om.GetManagedObjects()
    for o, props in objects.items():
        if LE_ADVERTISING_MANAGER_IFACE in props and GATT_MANAGER_IFACE in props:
            return o
        print('Skip adapter:', o)
    return None

def main():

    global mainloop
    global adv

    dbus.mainloop.glib.DBusGMainLoop(set_as_default=True)
    bus = dbus.SystemBus()
    adapter = find_adapter(bus)
    if not adapter:
        print('BLE adapter not found')
        return
 
    service_manager = dbus.Interface(
                                bus.get_object(BLUEZ_SERVICE_NAME, adapter),
                                GATT_MANAGER_IFACE)
    ad_manager = dbus.Interface(bus.get_object(BLUEZ_SERVICE_NAME, adapter),
                                LE_ADVERTISING_MANAGER_IFACE)
 
    app = BinbinApplication(bus)
    adv = BinbinAdvertisement(bus, 0)
 
    mainloop = GLib.MainLoop()
 
    service_manager.RegisterApplication(app.get_path(), {},
                                        reply_handler=register_app_cb,
                                        error_handler=register_app_error_cb)
    ad_manager.RegisterAdvertisement(adv.get_path(), {},
                                     reply_handler=register_ad_cb,
                                     error_handler=register_ad_error_cb)
    try:
        mainloop.run()
    except KeyboardInterrupt:
        mainloop.quit()
        adv.Release()
        bl = Bluetoothctl()
        bl.disconnect()
        print("Stop Binbin")
        pilotLamp.turn_off()

 
if __name__ == '__main__':
    main()