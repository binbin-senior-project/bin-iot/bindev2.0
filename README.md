# Binbin RVM System

## Installation

Raspberry Pi3:

```sh
pip3 install -r requirements.txt
```

## Structure

1. BLEUtility is for managing Bluetooth Low Energy 
2. BottleUtility is for managing Conveyer Belt, IR, Camera, Bin Selector, Compactor, and etc.
3. BinbinApplication2.py is the old main code.
4. BinbinApplication2.py is the newest main code.
5. BinbinHardwareTest.py is for testing with out bluetooth 

## Development setup

All processes (trash classification and bringing trash to correct bins) are working over bluetooth module

```sh
python3 BinbinApplication2.py
```
