# Build Path of libraries
import os
import sys
def get_parent_dir(n=1):
	current_path = os.path.dirname(os.path.abspath(__file__))
	for k in range(n):
		current_path = os.path.dirname(current_path)
	return current_path


bleUtil_path = os.path.join(get_parent_dir(0),'BLEUtility')
sys.path.append(bleUtil_path)
print("register path: ",bleUtil_path)


bottleUtil_path = os.path.join(get_parent_dir(0),'BottleUtility')
sys.path.append(bottleUtil_path)
print("register path: ",bottleUtil_path)
from PilotLamp import PilotLamp
pilotLamp = PilotLamp()


# import IR sensor & Conveyer Belt
from ConveyerBelt import findObject, inputObject, selectBin, measureSize

# import camera
from Camera import takePicture

from datetime import datetime

# import image classification
# from ImageClassification import detect_object, decode_result

import time

from BottleCompactor import Compactor
compactor = Compactor()
count = 1
try:
  print("READY")
  while True:
    # print("Wait for a object")
    pilotLamp.turn_on()
    inObject = inputObject()
    # print("Is object: "+str(inObject))
    if inObject == True:
      pilotLamp.turn_off()
      anyObjectOnThePosition = findObject()
      if anyObjectOnThePosition == True:

        st = time.time()           
        print("Detect Trash")
        # now = datetime.now()
        # date_time = now.strftime("%m.%d.%Y_%H.%M.%S")
        # img_path = 'TrashImages/Glass/glass' + str(count) + '.jpg'
        count += 1
        img_path = 'trash.jpg'
        takePicture(img_path)
        # preClass = detect_object(img_path)
        # trash, isObject = decode_result(preClass,0.999)
        # if 'glass' not in trash:
        compactor.turn_on() 
          
        # size = ''
        # if isObject:
        #     size = str(measureSize())
        #     print("Size",size)

        # trashR = trash + size          
        
        et = time.time()
        print()
        # print("The trash is",trashR.upper(),"Time spend:", et-st)
        print()
        selectBin('can')
        time.sleep(2)
        compactor.turn_off()

except KeyboardInterrupt:
    print("Stop Binbin")
    pilotLamp.turn_off()



      
